thecamp Docker LemonLDAP::NG Builds
===================================

![thecamp](https://gallery.mailchimp.com/15b3322baaead07a6033cee85/images/6abbb140-ce61-4d03-a99d-d28e272178be.png | width=200)
![LemonLDAP::NG](https://lemonldap-ng.org/welcome/logo_llng_600px.png | height=55)

This is an open source repository to share LemonLDAP::NG docker builds produced by thecamp.

See also:

 * https://lemonldap-ng.org/
 * https://thecamp.fr


# What's in the box ?

LemonLDAP::NG pre-configured to work with an LDAP server through a secured connection (ldap+tls)

 * LemonLDAP::NG 1.9.14 on Debian Jessie + apache2 (MPM prefork) = 389MB
 * LemonLDAP::NG 1.9.14 (without embedded documentation) on Centos 7 + apache2 (MPM prefork) = 548MB


# Compatibility

 * Mac OS X 10.10 or higher
 * Debian
 * Ubuntu
 * Windows (maybe, but untested)


## Features

 * Redis ready (packages are pre-installed, see further for conguration)
 * SOAP session endpoints are activated by default (getCookies, getAttributes...)
 * SAML signature activated by default and certificates are automatically configured
 * OpenID Connect certificate, public and private keys are automatically configured
 * apache and www-data linux's users has UID and GID 82: you should set this UID/GID on paths you want to share with the container within a volume mount. For sessions paths for instance, execute the following command on your host: `chown -R 82:82 path/to/lemonldap/sessions/files` and configure your volume's mount path in `docker-compose.yml`
 * supervisor is the main process in order to manage both apache2 and lemonldap cronjobs
 * Configurable using environment variables from `.env` file:
     * ENVIRONMENT=dev (will set apache2 log level to "debug")
     * DOMAIN_NAME=example
     * DOMAIN_EXTENSION=localhost
     * PROTOCOL=http
     * DOMAIN_CONTROLLER=your-ldap-host-or-ip
     * DOMAIN_ADMIN_USERNAME=your-ldap-username
     * DOMAIN_ADMIN_PASSWORD=your-ldap-password
     * MAILER_HOST=your-mailer-host-or-ip
     * MAILER_PORT=25
     * SESSION_LIFETIME=604800 (7 days)


# How to build images

(optional) You can create self-signed certificates in order to sign and secure SAML2 and OpenID requests, and put them in `builds/common/certificate/`.

You can also edit `builds/lemonldap/common/conf/1.9.x/lmConf-1.js`. For instance, to activate authentication against an Active Directory with LDAP protocol, replace or add those lines:

```json
  "authentication": "AD",
  "userDB": "AD",
  "passwordDB": "AD",
  "registerDB": "AD",
  "registerUrl": "$PROTOCOL://accounts.$DOMAIN_NAME.$DOMAIN_EXTENSION/register",
  "managerDn": "CN=$DOMAIN_ADMIN_USERNAME,DC=$DOMAIN_NAME,DC=$DOMAIN_EXTENSION",
  "managerPassword": "$DOMAIN_ADMIN_PASSWORD",
  "AuthLDAPFilter": "(&(|(sAMAccountName=$user)(mail=$user))(objectClass=person))",
  "ldapBase": "DC=$DOMAIN_NAME,DC=$DOMAIN_EXTENSION",
  "ldapServer": "ldap+tls://$DOMAIN_CONTROLLER",
```

Then you can build images with following commands:

```sh
docker-compose build lemonldap-debian-1.9.14
docker-compose build lemonldap-centos-1.9.14
```

# How to run container

First you have to build images on your computer.

Then you must edit `docker-compose.yml`:

 * Choose the image version you want, either `lemonldap-debian-1.9.14_image:latest` or `lemonldap-centos-1.9.14_image:latest` (by default)
 * Configure your LDAP SSL certificate path if you have one (for ldap+tls protocol)

You must also edit `.env` file and adapt it to your needs.

Then you can simply add lemonldap domains at the end of your `/etc/hosts` file:

```sh
127.0.0.1 auth.example.localhost manager.example.localhost test1.example.localhost test2.example.localhost
```

Or you might configure a reverse proxy such as traefik and configure a resolver and a dnsmasq (on MAC OSX for instance) to serve requests to lemonldap host.

Then start your container:

```sh
docker-compose up -d lemonldap
```

You can now access lemonldap by the following urls:

 * http://auth.example.localhost
 * http://manager.example.localhost (if logged in with an admin profile)


# How to stop container

```sh
docker-compose down
```

# Configuration tips

## Admin account

From json config file `builds/lemonldap/common/conf/1.9.x/lmConf-1.js`, indicate that only account with the username "$DOMAIN_ADMIN_USERNAME" can access manager interface

```json
  "macros": {
    "_whatToTraceAD": "$sAMAccountName"
  },
  "whatToTrace": "_whatToTraceAD",
  "locationRules": {
    "manager.$DOMAIN_NAME.$DOMAIN_EXTENSION": {
      "(?#Configuration)^/(manager\\.html|conf/)": "$sAMAccountName eq \"$DOMAIN_ADMIN_USERNAME\"",
      "(?#Notifications)/notifications": "$sAMAccountName eq \"$DOMAIN_ADMIN_USERNAME\"",
      "(?#Sessions)/sessions": "$sAMAccountName eq \"$DOMAIN_ADMIN_USERNAME\"",
      "default": "$sAMAccountName eq \"$DOMAIN_ADMIN_USERNAME\""
    }
  },
  "managerDn": "CN=$DOMAIN_ADMIN_USERNAME,DC=$DOMAIN_NAME,DC=$DOMAIN_EXTENSION",
  "managerPassword": "$DOMAIN_ADMIN_PASSWORD",
```

## Session storage

### File storage (default)

```json
"globalStorage": "Apache::Session::File",
"globalStorageOptions": {
    "Directory": "/var/lib/lemonldap-ng/sessions",
    "LockDirectory": "/var/lib/lemonldap-ng/sessions/lock",
    "generateModule": "Lemonldap::NG::Common::Apache::Session::Generate::SHA256"
}
```

### Redis on Centos

#### With package `perl-Apache-Session-NoSQL-0.2` (lemonldap-ng-extras) + `perl-Redis` (epel)

Use config:

``` json
"globalStorage": "Apache::Session::NoSQL",
"globalStorageOptions":{
    "Driver": "Redis",
    "server": "redis.$DOMAIN_NAME.$DOMAIN_EXTENSION:6379",
    "generateModule": "Lemonldap::NG::Common::Apache::Session::Generate::SHA256"
}
```

#### With package `perl-Apache-Session-Browseable` (lemonldap-ng-extras) + `perl-Redis` (epel) => recommended

Use config:

``` json
"globalStorage": "Apache::Session::Browseable::Redis",
"globalStorageOptions":{
    "Driver": "Redis",
    "server": "redis.$DOMAIN_NAME.$DOMAIN_EXTENSION:6379",
    "Index": "_whatToTrace ipAddr",
    "generateModule": "Lemonldap::NG::Common::Apache::Session::Generate::SHA256"
}
```

### Redis on Debian

#### With package `libapache-session-browseable-perl` + `libredis-perl`

Use config:

``` json
"globalStorage": "Apache::Session::Browseable::Redis",
"globalStorageOptions":{
    "Driver": "Redis",
    "server": "redis.$DOMAIN_NAME.$DOMAIN_EXTENSION:6379",
    "Index": "_whatToTrace ipAddr",
    "generateModule": "Lemonldap::NG::Common::Apache::Session::Generate::SHA256"
}
```

## OpenID Connect

### Generate Client ID / secret / signing key ID

see https://www.oauth.com/oauth2-servers/client-registration/client-id-secret/

Client ID

```sh
php -r "echo uniqid();"
```

Client secret

```sh
php -r "echo bin2hex(random_bytes(32));"
```

Signing Key ID

```sh
php -r "echo bin2hex(random_bytes(8));"
```

## SSL self-signed certificates

Generate a private key

```sh
openssl genrsa -out signature.key 2048
```

Generate a public key from private key

```sh
openssl rsa -in signature.key -pubout -out signature.pub
```

Generate a certificate for 5 years

```sh
openssl req \
    -newkey rsa:2048 -new -x509 -days 1825 -nodes -out signature.crt -keyout signature.key \
    -subj '/C=FR/ST=Bouches-du-Rhone/L=Aix-en-Provence/O=thecamp/OU=IT Department/CN=*'
```
