{
   "applicationList": {
      "1sample": {
         "catname": "Sample applications",
         "test1": {
            "options": {
               "description": "A simple application displaying authenticated user",
               "display": "auto",
               "logo": "demo.png",
               "name": "Application Test 1",
               "uri": "$PROTOCOL://test1.$DOMAIN_NAME.$DOMAIN_EXTENSION/"
            },
            "type": "application"
         },
         "test2": {
            "options": {
               "description": "The same simple application displaying authenticated user",
               "display": "auto",
               "logo": "thumbnail.png",
               "name": "Application Test 2",
               "uri": "$PROTOCOL://test2.$DOMAIN_NAME.$DOMAIN_EXTENSION/"
            },
            "type": "application"
         },
         "type": "category"
      },
      "2administration": {
         "catname": "Administration",
         "manager": {
            "options": {
               "description": "Configure LemonLDAP::NG WebSSO",
               "display": "auto",
               "logo": "configure.png",
               "name": "WebSSO Manager",
               "uri": "$PROTOCOL://manager.$DOMAIN_NAME.$DOMAIN_EXTENSION/manager.html"
            },
            "type": "application"
         },
         "notifications": {
            "options": {
               "description": "Explore WebSSO notifications",
               "display": "auto",
               "logo": "database.png",
               "name": "Notifications explorer",
               "uri": "$PROTOCOL://manager.$DOMAIN_NAME.$DOMAIN_EXTENSION/notifications.html"
            },
            "type": "application"
         },
         "sessions": {
            "options": {
               "description": "Explore WebSSO sessions",
               "display": "auto",
               "logo": "database.png",
               "name": "Sessions explorer",
               "uri": "$PROTOCOL://manager.$DOMAIN_NAME.$DOMAIN_EXTENSION/sessions.html"
            },
            "type": "application"
         },
         "type": "category"
      },
      "3documentation": {
         "catname": "Documentation",
         "localdoc": {
            "options": {
               "description": "Documentation supplied with LemonLDAP::NG",
               "display": "on",
               "logo": "help.png",
               "name": "Local documentation",
               "uri": "$PROTOCOL://manager.$DOMAIN_NAME.$DOMAIN_EXTENSION/doc/"
            },
            "type": "application"
         },
         "officialwebsite": {
            "options": {
               "description": "Official LemonLDAP::NG Website",
               "display": "on",
               "logo": "network.png",
               "name": "Offical Website",
               "uri": "http://lemonldap-ng.org/"
            },
            "type": "application"
         },
         "type": "category"
      }
   },
   "authentication": "Demo",
   "cfgAuthor": "The LemonLDAP::NG team",
   "cfgNum": 1,
   "cookieName": "lemonldap",
   "demoExportedVars": {
      "cn": "cn",
      "mail": "mail",
      "uid": "uid"
   },
   "domain": "$DOMAIN_NAME.$DOMAIN_EXTENSION",
   "exportedHeaders": {
      "test1.$DOMAIN_NAME.$DOMAIN_EXTENSION": {
         "Auth-User": "$uid"
      },
      "test2.$DOMAIN_NAME.$DOMAIN_EXTENSION": {
         "Auth-User": "$uid"
      }
   },
   "exportedVars": {
      "UA": "HTTP_USER_AGENT"
   },
   "globalStorage": "Apache::Session::File",
   "globalStorageOptions": {
      "Directory": "/var/lib/lemonldap-ng/sessions",
      "LockDirectory": "/var/lib/lemonldap-ng/sessions/lock",
      "generateModule": "Lemonldap::NG::Common::Apache::Session::Generate::SHA256"
   },
   "groups": {},
   "localSessionStorage": "Cache::FileCache",
   "localSessionStorageOptions": {
      "cache_depth": 3,
      "cache_root": "/tmp",
      "default_expires_in": 600,
      "directory_umask": "007",
      "namespace": "lemonldap-ng-sessions"
   },
   "locationRules": {
      "manager.$DOMAIN_NAME.$DOMAIN_EXTENSION": {
         "(?#Configuration)^/(manager\\.html|conf/)": "$uid eq \"dwho\"",
         "(?#Notifications)/notifications": "$uid eq \"dwho\" or $uid eq \"rtyler\"",
         "(?#Sessions)/sessions": "$uid eq \"dwho\" or $uid eq \"rtyler\"",
         "default": "$uid eq \"dwho\""
      },
      "test1.$DOMAIN_NAME.$DOMAIN_EXTENSION": {
         "^/logout": "logout_sso",
         "default": "accept"
      },
      "test2.$DOMAIN_NAME.$DOMAIN_EXTENSION": {
         "^/logout": "logout_sso",
         "default": "accept"
      }
   },
   "loginHistoryEnabled": 1,
   "macros": {
      "_whatToTrace": "$_auth eq 'SAML' ? \"$_user\\@$_idpConfKey\": \"$_user\""
   },
   "mailUrl": "$PROTOCOL://auth.$DOMAIN_NAME.$DOMAIN_EXTENSION/mail.pl",
   "mailFrom": "noreply@$DOMAIN_NAME.$DOMAIN_EXTENSION",
   "notification": 1,
   "notificationStorage": "File",
   "notificationStorageOptions": {
      "dirName": "/var/lib/lemonldap-ng/notifications"
   },
   "passwordDB": "Demo",
   "persistentStorage": "Apache::Session::File",
   "persistentStorageOptions": {
      "Directory": "/var/lib/lemonldap-ng/psessions",
      "LockDirectory": "/var/lib/lemonldap-ng/psessions/lock"
   },
   "portal": "$PROTOCOL://auth.$DOMAIN_NAME.$DOMAIN_EXTENSION/",
   "portalSkin": "bootstrap",
   "portalSkinBackground": "1280px-Cedar_Breaks_National_Monument_partially.jpg",
   "registerDB": "Demo",
   "registerUrl": "$PROTOCOL://auth.$DOMAIN_NAME.$DOMAIN_EXTENSION/register.pl",
   "reloadUrls": {
      "reload.$DOMAIN_NAME.$DOMAIN_EXTENSION": "$PROTOCOL://reload.$DOMAIN_NAME.$DOMAIN_EXTENSION/reload"
   },
   "securedCookie": 0,
   "sessionDataToRemember": {},
   "timeout": $SESSION_LIFETIME,
   "userDB": "Demo",
   "whatToTrace": "_whatToTrace",
   "Soap": 1,
   "SMTPServer": "$MAILER_HOST:$MAILER_PORT",
   "issuerDBOpenIDConnectActivation": 1,
   "issuerDBOpenIDConnectRule": 1,
   "oidcServiceMetaDataIssuer": "$PROTOCOL://auth.$DOMAIN_NAME.$DOMAIN_EXTENSION",
   "oidcServiceAllowAuthorizationCodeFlow": 1,
   "oidcServiceAllowHybridFlow": 0,
   "oidcServiceAllowImplicitFlow": 0,
   "oidcServicePublicKeySig": "----- will be replaced -----",
   "oidcServicePrivateKeySig": "----- will be replaced -----",
   "issuerDBSAMLActivation": 1,
   "issuerDBSAMLRule": 1,
   "samlServicePublicKeySig": "----- will be replaced -----",
   "samlServicePrivateKeySig": "----- will be replaced -----",
   "samlServiceUseCertificateInResponse": 1,
   "samlIDPSSODescriptorWantAuthnRequestsSigned": 1,
   "samlSPSSODescriptorAuthnRequestsSigned": 1,
   "samlSPSSODescriptorWantAssertionsSigned": 1
}
